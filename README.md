# Bitcoin RNN prediction

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/vachmara/Bitcoin-price-RNN/-/blob/84d11bbbfabe182d2016b9ccfe28378fbd95d46e/LICENSE)
[![Open In Collab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1L4aOzoPhcrT-s0UiE9J1Fzm5AqqX8GsD?usp=sharing)

The goal of this project is to build any cryptocurrency prediction model easily. Dataset creation, model training and model prediction used, is explained step by step. 


**Summary :**  
  1. Environment set up 
  2. Bitcoin Dataset creation
  3. RNN predictive model
  4. Bibliography

# 1. Environment set up

If you want to use CUDA processor, please execute scripts corresponding to your os in `scripts/`.

```shell
git clone https://gitlab.com/vachmara/Bitcoin-price-RNN.git
cd Bitcoin-price-RNN
pip install requierments.txt
jupyter notebook
```

Go to `notebook/main.ipynb` if you want rapidly build a dataset and a predictive model.

# 2. Bitcoin Dataset creation

First, a financial dataset corresponding to the cryptocurrency you want to predict need to be fetched, organized and checked. You will find a [Jupyter notebook](notebook/Binance_trend_data.ipynb), `notebook/Binance_trend_data.ipynb` (also available in [Colab](https://colab.research.google.com/drive/1L4aOzoPhcrT-s0UiE9J1Fzm5AqqX8GsD?usp=sharing)) used to build the source file `src/dataset.py`. Note that all data is from **Binance API**.

Bellow are the constant in `dataset.py` module that you could change:
- `PAIR`: ***str*** -- Cryptocurrency pair, `BTCUSDT` by default.
- `INTERVAL` : ***str*** -- The interval time candle fetch, `"1h"` by default.
- `INTERVAL_MS` : ***int*** -- The interval equivalent in ***ms***, `int(60 * 60 * 1e6)` by default.
- `KW_LIST` : ***list*** -- Keyword list use to fetch the interest percentage in Google search, `["Bitcoin"]` by default.
  
To generate your financial dataset use `create_dataset( start: Date, end: Date, pair=None, kw_list=KW_LIST, path=None) -> pd.DataFrame` function. Bellow the arguments needed : 
- `start` : ***Date*** -- Start fetching date.
- `end` : ***Date*** -- End fetching date.
- `pair` : ***str*** -- Cryptocurrency pair fetched; optional arg, `PAIR` by default.
- `kw_list` : ***list*** -- Keyword list; optional arg, `KW_LIST` by default.
- `path` : ***str*** -- Path to save the DataFrame in CSV format; optional arg, not saved if None.
 

*Possible integration for the future (format need to be defined to correctly fit into the prediction model):*
  
  3. *Liquidation price*
  4. *Bid / ask* 
 
# 3. RNN predictive model

Tensorflow has been used to build predictive model. The architecture will be presented below, all documentation and articles used to build it are stored in [4. Resource and documentation](#4-bibliography) section. You will find a [Jupyter notebook](notebook/RNN_LSTM_model.ipynb), `notebook/RNN_LSTM_model.ipynb` (also available in [Colab](https://colab.research.google.com/drive/1r47MI797gNJMmje0_A_oY3KIeD24y_rh?usp=sharing) used to build the source file `src/build_pred_model.py`.


`BuildModel` is a class that can simply create and train a cryptocurrency predictive model. You can also save or load a model created. Find bellow the arguments necessary to use this Object:
- `path_csv` : ***str*** -- Path where is your cryptocurrency financial csv (created with the previous module). 
- `model_name` : ***str*** -- Name used to load or saved a predictive model. 
- `load` : ***bool*** -- If True, the `model_name` will be searched and loaded if found in `saved_model/` repository; False by default.

Find bellow a usage example: 
```python
from src.build_pred_model import BuildModel
build_model = BuildModel("dataset/BTCUSDT_1594944000000_1627776000000.csv", "test")
build_model.build_model()
build_model.train_model()
build_model.evaluate_model()
build_model.save_model()
```
Your model will be saved in `saved_model/test/`.

# 4. Bibliography

1. Article written by Venelin Valkov: [Cryptocurrency price prediction using LSTMs | TensorFlow for Hackers  ](https://towardsdatascience.com/cryptocurrency-price-prediction-using-lstms-tensorflow-for-hackers-part-iii-264fcdbccd3f)
2. Online courses presented by Alexander Amini and Ava Soleimany : [MIT Deep Learning 6.S191](http://introtodeeplearning.com/)
3. Documentation : [Guide | Tensorflow Core](https://www.tensorflow.org/guide/)
