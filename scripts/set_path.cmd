@echo off
REM Setup for Windows from https://www.tensorflow.org/install/gpu
SET PATH=C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.4\bin;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.4\extras;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.4\include;%PATH%
Echo PATH updated