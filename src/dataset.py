"""
Author : Valentin Chmara
Email  : valentinchmara@gmail.com

Dataset functions and class.
Used to scrap, verify and format crypto financial dataset.


Constant
------------------

PAIR            -- string   : Put a listed Binance pair
INTERVAL        -- string   : The time interval used to fetch info via Binance API
INTERVAL_MS     -- int      : The integer number equivalent to INTERVAL (example 1h = 60 * 60 * 1000 ms) 
BINANCE_HEADER  -- list     : The result header from Binance API. You shouldn't change this const.

Instance
------------------

client          -- Spot     : binance spot object to simplify Binance API requests
pytrend         -- TrendReq : pytrend object to simplify Google API requests

Class
------------------

Date            -- Date     : Stock easily date format and transform into ms timestamp

Functions
------------------

_check_data(df)                             -> DataFrame
get_data_klines(start, end)                 -> DataFrame
get_klines(start, end)                      -> DataFrame
test_time_dfs(df1, df2)                     -> None
formatting(dfs, cols)                       -> DataFrame
create_dataset( start: Date, end: Date, pair=None,
                kw_list=KW_LIST, path=None) -> DataFrame

"""

import datetime
import os
from tqdm import tqdm
from pandas import DataFrame, concat
from pytrends.request import TrendReq
from binance.spot import Spot

# Binance data
PAIR = "BTCUSDT"
INTERVAL = "1h"
INTERVAL_MS = int(60 * 60 * 1e6)
BINANCE_HEADER = ["Open time", "Open price", "High price", "Low price", "Close price", "Volume", "Close time",
                  "Quote asset volume", "Number of trades", "Taker buy base asset vol", "Taker buy quote asset vol",
                  "Ignore"]

# Google trend data
KW_LIST = ["Bitcoin"]

client = Spot()
pytrend = TrendReq()


# Class definition

class Date:
    """
    Store a date, transform into ms timestamp

    Arguments
    ------------------

    year    -- int : Year parameter
    month   -- int : Month parameter
    day     -- int : Day parameter
    hour    -- int : Hour parameter
    min     -- int : Minutes optional parameter

    Functions
    ------------------

    get_timestamp() -- int : Return a ms timestamp
    print           -- None : Print the date into the class

    """

    def __init__(self, year, month, day, hour, minutes=None):
        self.y = year
        self.m = month
        self.d = day
        self.h = hour
        self.min = 0 if minutes is None else minutes

    def get_timestamp(self):
        return int(datetime.datetime(self.y, self.m, self.d, self.h, self.min).timestamp() * 1000)

    def print(self):
        print("{}/{:02d}/{:02d} - {:02d}:{:02d}".format(self.y, self.m, self.d, self.h, self.min))


# Function definition

def get_data_klines(start, end) -> DataFrame:
    """
    Multiple get_klines calls (caused by the API limitation) to fetch Binance Klines data from start date to end date.
    
    Arguments :
        start -- Date  
        end   -- Date 
    
    Return
        df    -- DataFrame
    """
    s = start.get_timestamp()
    e = end.get_timestamp()
    df = DataFrame([], columns=BINANCE_HEADER)

    for t in range(s, e, INTERVAL_MS):
        if t + INTERVAL_MS <= e:
            df = concat([df, get_klines(t, t + INTERVAL_MS)], ignore_index=True)
        else:
            df = concat([df, get_klines(t, e)], ignore_index=True)

    return _check_data(df.drop_duplicates(subset=["Open time"]).reset_index(drop=True))


# noinspection PyArgumentList
def get_klines(start, end) -> DataFrame:
    """
    Fetch Binance Klines. Start and end date must represent at most one thousand interval time.

    Arguments :
        start -- Date  
        end   -- Date 
    
    Return
        df    -- DataFrame
    """
    return DataFrame(client.klines(symbol=PAIR, interval=INTERVAL, limit=1000, startTime=start, endTime=end),
                     columns=BINANCE_HEADER)


def get_data_trend(start, end) -> DataFrame:
    """
    Fetch Google search trend data.

    Arguments :
        start -- Date  
        end   -- Date 
    
    Return
        df    -- DataFrame
    """
    df = pytrend.get_historical_interest(KW_LIST, start.y, start.m, start.d, start.h, end.y, end.m, end.d, end.h)
    df["Open time"] = [int(float(d.timestamp()) * 1000) for d in df.index]
    return _check_data(df.drop_duplicates(subset=["Open time"]).reset_index(drop=True))


def test_time_dfs(df1, df2):
    """
    Checking the correlation between each "Open time" data from two dfs.
    Print the row where the error happen if there is one.

    Arguments :
        df1   -- DataFrame
        df2   -- DataFrame
    
    """
    size = len(df2.index) if len(df2.index) < len(df1.index) else len(df1.index)
    for i in range(size):
        o1 = int(df1["Open time"][i])
        o2 = int(df2["Open time"][i])
        if o1 != o2:
            print(i, o1, o2)
            break


def _check_data(df) -> DataFrame:
    """
    Check and fix Open time data .
    
    Arguments :
        df    -- DataFrame
    
    Return 
        df    -- DataFrame

    """

    for i in tqdm(range(len(df.index) - 1)):
        o1 = df["Open time"][i] + int(INTERVAL_MS / 1e3)
        o2 = df["Open time"][i + 1]

        if o1 != o2:  # Check if the i and i+1 data are consistent
            s = df.copy().xs(i)
            for inter in range(o1, o2, int(INTERVAL_MS / 1e3)):  # Loop to prevent multiple missing data
                s["Open time"] = inter
                for c in ["Volume", "Quote asset volume", "Number of trades", "Taker buy base asset vol",
                          "Taker buy quote asset vol", "Close time"]:
                    if c in df.columns and c != "Close time":
                        s[c] = 0
                    elif c in df.columns and c == "Close time":
                        s[c] = inter - 1 + int(INTERVAL_MS / 1e3)
                df = concat([df[:i], DataFrame(dict(s), columns=df.columns, index=[len(df.columns)]), df[i:]])

    return df.sort_values(by=["Open time"]).reset_index(drop=True)


def formatting(dfs, cols) -> DataFrame:
    """
    Formatting the final DataFrame. Dfs is a list with one or multiple DataFrame to be concatenated. 
    Cols is a list of the columns required in the final DataFrame. 
    
    Arguments :
        dfs     -- DataFrame
        cols    -- list
    Return 
        df      -- DataFrame

    """
    final_df = DataFrame()

    for df in dfs:
        for col in df.columns:
            if col in cols and col not in final_df.columns:
                final_df[col] = df[col].copy()

    return final_df


def create_dataset(start: Date, end: Date, pair=None, kw_list=None, path=None) -> DataFrame:
    """
    Create a dataset to be used into the RNN model. Dataset will be save in path onto csv format, if it is specified.

    Arguments :
        start       -- Date
        end         -- Date
        pair        -- string
        kw_list     -- list
        path        -- string
    Return 
        df          -- DataFrame

    """

    if kw_list is None:
        kw_list = KW_LIST

    if pair is None:
        pair = PAIR
    print("{} fetching from {} to {}.\nKeyword used with google trend : {}.".format(pair, start.get_timestamp(),
                                                                                    end.get_timestamp(), kw_list))
    df_klines = get_data_klines(start, end)
    df_trend = get_data_trend(start, end)

    df = formatting([df_klines, df_trend],
                    ["Open time", "Open price", "High price", "Low price", "Close price", "Volume",
                     "Number of trades"] + kw_list)

    if path:
        if not os.path.exists(path):
            raise Exception("Please enter an existing directory.")
        df.to_csv("{}/{}_{:d}_{:d}.csv".format(path, pair, start.get_timestamp(), end.get_timestamp()), index=False)

    return df
