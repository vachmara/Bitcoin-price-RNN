"""
Author : Valentin Chmara
Email  : valentinchmara@gmail.com

Prediction model building class and function

Class
------------------

BuildModel                      --  BuildModel : Compute a cryptocurrency Long Short Term Memory model.

Functions
------------------

to_sequences(data, seq_len)     -> np.array

"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import os

from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, LSTM, Dropout, Bidirectional


# Class definition
class BuildModel:
    """
    Class to train, save, load a Long Short Term Memory

    Arguments
    ------------------

    path_csv        -- str  : Path of the cryptocurrency csv
    model_name      -- str  : Name to save or load model
    load            -- bool : Create a new model or load one

    Functions
    ------------------

    open_csv(self)          -> pd.DataFrame
    build_model(self)       -> None
    train_model(self)       -> None
    evaluate_model(self)    -> None
    save_model(self)        -> None
    test_prediction(self)   -> None
    _normalization(self)    -> None
    _create_model(self)     -> None
    """

    def __init__(self, path_csv, model_name, load=False):
        # Const
        self.SEQ_LEN = 100
        self.TRAIN_SPLIT = 0.9
        self.BATCH_SIZE = 64  # See Tensorflow fit model arguments
        self.EPOCHS = 50
        self.VALIDATION_SPLIT = 0.15

        # Args
        self.path_in = path_csv
        self.path_out = '{}\saved_model\{}'.format(os.getcwd(), model_name)
        self.loaded = load
        self.model = Sequential() if not self.loaded else tf.keras.models.load_model(self.path_out)

        # Init
        self.scaler = MinMaxScaler()
        self.training_data = None
        self.history, self.df = None, None
        self.X_train, self.y_train, self.X_test, self.y_test = None, None, None, None
        self.loss, self.acc = None, None

    def open_csv(self) -> pd.DataFrame:
        """
        Open dataframe from csv file.
        Columns required :
            "Open time", "Open price", "High price", "Low price",
            "Close price", "Volume", "Number of trades", "Google trends data"
        """
        return pd.read_csv(self.path_in)

    def build_model(self):
        """
        Build the model
        """
        self.df = self.open_csv()
        self.training_data = self.scaler.fit_transform(self.df.drop(["Open time"], axis=1).fillna(0))
        self._normalization()
        self._create_model()
        self.model.summary()

    def train_model(self):
        """
        Train the model
        """
        self.history = self.model.fit(self.X_train,
                                      self.y_train,
                                      epochs=self.EPOCHS,
                                      batch_size=self.BATCH_SIZE,
                                      validation_split=self.VALIDATION_SPLIT)

    def evaluate_model(self):
        """
        Evaluate the model. Plot the loss and accuracy.
        """
        self.loss, self.acc = self.model.evaluate(self.X_test, self.y_test)

        plt.plot(self.history.history['loss'])
        plt.plot(self.history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()

    def test_prediction(self):
        """
        Plot predicted price / real price.
        Testing of the train model. Depends on the TRAIN_SPLIT variable.
        """

        y_pred = self.model.predict(self.X_test)

        y_test_inverse = self.scaler.inverse_transform(self.y_test)
        y_pred_inverse = self.scaler.inverse_transform(y_pred)

        plt.plot(y_test_inverse[:, 0], label="Actual Price", color='green')
        plt.plot(y_pred_inverse[:, 0], label="Predicted Price", color='red')

        plt.title('Price prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend(loc='best')

        plt.show()

    def save_model(self):
        """
        Save model at self.path_out
        """
        if not self.loaded:
            self.model.save(self.path_out)

    def _normalization(self):
        """
        Data normalization
        """
        data = to_sequences(self.training_data, self.SEQ_LEN)
        num_train = int(self.TRAIN_SPLIT * data.shape[0])

        self.X_train = data[:num_train, :-1, :]
        self.y_train = data[:num_train, -1, :]
        self.X_test = data[num_train:, :-1, :]
        self.y_test = data[num_train:, -1, :]

    def _create_model(self):
        """
        Add model LSTM and Dropout Layer
        """
        self.model.add(Bidirectional(LSTM(self.SEQ_LEN - 1, return_sequences=True),
                                     input_shape=(self.SEQ_LEN - 1, self.X_train.shape[0])))
        self.model.add(Dropout(rate=0.4))
        self.model.add(Bidirectional(LSTM((self.SEQ_LEN - 1) * 2, return_sequences=True)))
        self.model.add(Dropout(rate=0.2))
        self.model.add(Bidirectional(LSTM(self.SEQ_LEN - 1, return_sequences=False)))
        self.model.add(Dense(units=7))

        self.model.compile(optimizer="adam", loss="mean_squared_error", metrics=['accuracy'])


# Functions definitions
def to_sequences(data, seq_len) -> np.array:
    """
    Data sequences

    Arguments :
        data    -- np.array
        seq_len -- int

    Return
        array   -- np.array
    """
    return np.array([data[i:i + seq_len] for i in range(len(data) - seq_len)])
